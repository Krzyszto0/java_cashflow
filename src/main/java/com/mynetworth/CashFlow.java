package com.mynetworth;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

public class CashFlow {

  @FXML
  private MenuButton chooseCategory;

  @FXML
  private MenuButton chooseAccount;

  @FXML
  private TextField amountInput;

  private String selectedCategory;

  private String selectedAccount;

  @FXML
  private void switchToHome() throws IOException {
    App.setRoot("home");
  }

  public void choosenCategory(ActionEvent actionEvent) {
    MenuItem selectedMenuItem = (MenuItem) actionEvent.getSource();
    selectedCategory = selectedMenuItem.getText();
    chooseCategory.setText(selectedCategory);
  }

  public void choosenAccount(ActionEvent event) {
    MenuItem selectedMenuItem = (MenuItem) event.getSource();
    selectedAccount = selectedMenuItem.getText();
    chooseAccount.setText(selectedAccount);
  }

  public void addIncome() {
    DatabaseManager databaseManager = new DatabaseManager();
    double amount = Double.parseDouble(amountInput.getText());
    databaseManager.addTransaction(selectedCategory, amount, selectedAccount);
  }

  @FXML
  public void initialize() {
    DatabaseManager databaseManager = new DatabaseManager();
    databaseManager.getAccount();
    for (String account : databaseManager.accounts) {
      MenuItem menuItem = new MenuItem(account);
      menuItem.setOnAction(event -> choosenAccount(event));
      chooseAccount.getItems().add(menuItem);
    }
  }
}
