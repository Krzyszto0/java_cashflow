package com.mynetworth;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class home {

  @FXML
  private void switchToSecondary() throws IOException {
    App.setRoot("secondary");
  }

  @FXML
  private TextField newAccount;

  public void insertAccount() {
    DatabaseManager databaseManager = new DatabaseManager();
    String accountName = newAccount.getText();
    databaseManager.addAccount(accountName);
  }
}
