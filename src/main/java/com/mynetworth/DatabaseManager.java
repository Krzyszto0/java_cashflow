package com.mynetworth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

  private static final String DB_URL = "jdbc:mysql://localhost/Finances";
  private static final String DB_USER = "Username";
  private static final String DB_PASSWORD = "ycG2*lA!6";

  public List<String> accounts = new ArrayList<>();

  public void addTransaction(String category, double amount, String account) {
    try (
      Connection connection = DriverManager.getConnection(
        DB_URL,
        DB_USER,
        DB_PASSWORD
      )
    ) {
      String query =
        "INSERT INTO Income (category, amount, account, transfer) VALUES (?, ?, ?, ?)";
      PreparedStatement preparedStatement = connection.prepareStatement(query);
      preparedStatement.setString(1, category);
      preparedStatement.setDouble(2, amount);
      preparedStatement.setString(3, account);
      preparedStatement.setBoolean(4, false);
      preparedStatement.executeUpdate();
      System.out.println("Dodano transakcję do bazy danych.");
    } catch (SQLException e) {
      System.err.println(
        "Błąd podczas dodawania transakcji do bazy danych: " + e.getMessage()
      );
    }
  }

  public void addAccount(String accountName) {
    try (
      Connection connection = DriverManager.getConnection(
        DB_URL,
        DB_USER,
        DB_PASSWORD
      )
    ) {
      String query =
        "INSERT INTO Accounts (account_name, daily_account) VALUES (?, ?)";
      PreparedStatement preparedStatement = connection.prepareStatement(query);
      preparedStatement.setString(1, accountName);
      preparedStatement.setBoolean(2, true);
      preparedStatement.executeUpdate();
      System.out.println("Dodano transakcję do bazy danych.");
    } catch (SQLException e) {
      System.err.println(
        "Błąd podczas dodawania transakcji do bazy danych: " + e.getMessage()
      );
    }
  }

  public void getAccount() {
    try (
      Connection connection = DriverManager.getConnection(
        DB_URL,
        DB_USER,
        DB_PASSWORD
      )
    ) {
      String query = "SELECT account_name FROM Accounts";
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery(query);

      while (resultSet.next()) {
        String columnName = resultSet.getString("account_name");
        accounts.add(columnName);
      }
      System.out.println("Odebrano z Bazy");
    } catch (SQLException e) {
      System.err.println(
        "Błąd podzczas pobierania Bazy danych: " + e.getMessage()
      );
    }
  }
}
