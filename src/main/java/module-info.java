module com.mynetworth {
  requires javafx.controls;
  requires javafx.fxml;
  requires java.sql;
  requires java.base;
  requires transitive javafx.base;
  requires transitive javafx.graphics;

  opens com.mynetworth to javafx.fxml;
  exports com.mynetworth ;
}
